//https://github.com/floatinghotpot/cordova-admob-pro#quick-start
var admobid = {};
if (/(android)/i.test(navigator.userAgent)) {
    admobid = { // for Android , banner not used
        banner: 'ca-app-pub-aaaaaaaaaaaaa',
        interstitial: 'ca-app-pub-3654628576200837/1882169994'
    };
} else if (/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
    admobid = { // for iOS
        banner: 'ca-app-pub-xxxxxx',
        interstitial: 'ca-app-pub-xxxxx'
    };
} else {
    admobid = { // for Windows Phone
        banner: 'ca-app-pub-yyyyyy',
        interstitial: 'ca-app-pub-yyyyy'
    };
}

function prepareAds() {
    console.log("prepareAds starting..");
  if (! AdMob ) { alert( 'admob plugin not ready' ); return; }

  // this will load a full screen ad on startup
  AdMob.prepareInterstitial({
    adId: admobid.interstitial,
    isTesting: false, // TODO: remove this line when release
    autoShow: false //tested the ad IS displayed however it isn't in this game. 
  });
    console.log("prepareAds ended..");
}

if(( /(ipad|iphone|ipod|android|windows phone)/i.test(navigator.userAgent) )) {
    document.addEventListener('deviceready', prepareAds, false);
} else {
    prepareAds();
}

//From
//https://github.com/floatinghotpot/cordova-admob-pro/blob/master/test/index.html
$(document).on('onAdFailLoad', function(e){
      // when jquery used, it will hijack the event, so we have to get data from original event
      if(typeof e.originalEvent !== 'undefined') e = e.originalEvent;
      var data = e.detail || e.data || e;
      //if (data.error==0) return;
      alert('error: ' + data.error +
          ', reason: ' + data.reason +
          ', adNetwork:' + data.adNetwork +
          ', adType:' + data.adType +
          ', adEvent:' + data.adEvent); // adType: 'banner', 'interstitial', etc.
    });

$(document).on('onAdDismiss'), function(e) {
    if (getParamValue('ctl-arcade') === "true") {
        parent.__ctlArcadeEndLevel({level: iLevel});
    }
   // prepareAds();
  }